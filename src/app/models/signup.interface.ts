export interface User {
    email: string;
    confirmEmail: string;
    password: string;
    confirmPassword: string;
    user_rol: string;
}
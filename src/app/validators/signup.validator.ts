import { FormGroup } from '@angular/forms';

export function passwordMatch(fg: FormGroup) {
    const password = fg.get('password').value;
    const passwordConfirm = fg.get('confirmPassword').value;
    
    if (!password || !passwordConfirm) { return null; }
    
    return password === passwordConfirm ? null : { notSame: true };
}

export function emailMatch(fg: FormGroup) {
    const email = fg.get('email').value;
    const confirmEmail = fg.get('confirmEmail').value;
    
    if (!email || !confirmEmail) { return null; }
    
    return email === confirmEmail ? null : { notSame: true };
}

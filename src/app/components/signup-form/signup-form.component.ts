import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from '../../models/signup.interface';
import { passwordMatch } from '../../validators/signup.validator';

@Component({
  selector: 'app-signup-form',
  templateUrl: './signup-form.component.html',
  styles: []
})
export class SignupFormComponent implements OnInit {
  user: FormGroup;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.user = this.fb.group({
      email: ['', [Validators.required, Validators.minLength(8)]],
      confirmEmail: ['', [Validators.required, Validators.minLength(8)]],
      passwordGroup: this.fb.group({
        password: ['', [Validators.required]],
        confirmPassword: ['', [Validators.required]]
      }, { validator: passwordMatch }),
      user_rol: ['', [Validators.required]]
    });
  }

  onSubmit() {
    console.log(this.user);
  }

}
